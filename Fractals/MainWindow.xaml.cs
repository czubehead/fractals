﻿using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Fractals
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private WriteableBitmap _bmp;

        public MainWindow()
        {
            Loaded += OnLoaded;
            InitializeComponent();
        }

        private void OnLoaded(object sender, EventArgs eventArgs)
        {
            var w = (int)MeasureRectangle.ActualWidth;
            var h = (int)MeasureRectangle.ActualHeight;
            _bmp = new WriteableBitmap(w, h,
                96,
                96,
                PixelFormats.Bgra32,
                null);
            CanvasImage.Source = _bmp;
        }

        public void Start()
        {
            const int w=2;
            const int h = w;
            const int stride=100;
        }

        private void DrawPixel(int x, int y, Color color)
        {
            _bmp.Lock();

            var buff = _bmp.BackBuffer;
            int stride = _bmp.BackBufferStride;
            unsafe
            {
                byte* pBuff = (byte*)buff.ToPointer();

                int loc = y * stride + x * 4;
                pBuff[loc] = color.B;
                pBuff[loc + 1] = color.G;
                pBuff[loc + 2] = color.R;
                pBuff[loc + 3] = color.A;
            }

            _bmp.AddDirtyRect(new Int32Rect(x, y, 1, 1));
            _bmp.Unlock();
        }

        private void CommandBinding_Always(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CommandBinding_OnExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            var routed = e.Command as RoutedCommand;
            if (routed == null) return;

            if (routed.Name == MediaCommands.Play.Name) //begin
            {
                Start();
            }
        }
    }
}