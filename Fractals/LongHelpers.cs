﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace Fractals
{
    /// <summary>
    /// Provides helper methods for <see cref="long"/>
    /// </summary>
    public static class LongHelpers
    {
        /// <summary>
        /// Max digits <see cref="long"/> can hold
        /// </summary>
        public const int MaxDigits = 17;

        /// <summary>
        /// Returns whether 2 longs can be safely multiplied
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool CanMultiply(long a, long b)
        {
            if (a == 0 || b == 0) return true;
            long maxMultiplier = long.MaxValue / a;
            return b <= maxMultiplier;
        }

        /// <summary>
        ///     Returns 10 raised to the power of <paramref name="e" />
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        public static long Exp(int e)
        {
            if (e > MaxDigits)
                throw new ArgumentOutOfRangeException(nameof(e));
            var l = 1L;
            for (var i = 0; i < e; i++)
                l *= 10L;

            return l;
        }

        /// <summary>
        /// Converts <see cref="BigInteger"/> to <see cref="Number"/> by reducing its digit count
        /// </summary>
        /// <param name="big"></param>
        /// <returns></returns>
        public static Number ReduceToNumber(this BigInteger big)
        {
            var str = big.ToString(CultureInfo.InvariantCulture);
            if (big.Sign == -1)
                str = str.Substring(1);//get rid of minus

            if (str.Length <= MaxDigits)//no need to reduce digit count
                return Number.NewTrimmed(long.Parse(str) * big.Sign);

            //too long = length > MaxDigits
            string usablePart = str.Remove(MaxDigits + 1);
            string ditchedPart = str.Substring(MaxDigits + 1);
            long usableLong = long.Parse(usablePart) * big.Sign;
            //long about to be used as Integer

            int ditchedE = ditchedPart.Length;
            //ditched part digits

            #region round ditched part and increment usableLong if needed

            //how many digits of ditched part are significant for rounding
            const int significantDitched = 3;
            ditchedPart = ditchedPart.Length <= significantDitched
                ? ditchedPart
                : ditchedPart.Remove(significantDitched);
            int ditchedInt = int.Parse(ditchedPart);
            double firstDitched = Math.Round(ditchedInt * 1D / Exp(ditchedPart.Length - 1));
            //first ditched digit
            if ("56789".Contains(firstDitched.ToString(CultureInfo.InvariantCulture)))
                usableLong++;
            //first cut-off digit rounds usableLong up

            #endregion

            return new Number(usableLong, ditchedE);
        }
    }
}
