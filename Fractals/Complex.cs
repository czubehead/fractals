﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fractals
{
    /// <summary>
    /// Complex number representation
    /// </summary>
    public struct Complex
    {
        /// <summary>
        /// Gets or sets real part
        /// </summary>
        public Number A { get; set; }

        /// <summary>
        /// Gets or sets imaginary part
        /// </summary>
        public Number B { get; set; }

        /// <summary>
        /// Initializes new instance of <see cref="Complex"/>
        /// </summary>
        /// <param name="a">real part</param>
        /// <param name="b">imaginary part</param>
        public Complex(Number a, Number b)
        {
            A = a;
            B = b;
        }

        /// <summary>
        /// Returns a user friendly representation
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            var a = A.ToString();
            var b = B.ToString();
            var sign = '+';
            if (b[0] == '-')
                sign = '-';

            return $"{a}{sign}{b}i";
        }

        /// <summary>
        /// Returns absolute value of complex number using Pythagorean theorem 
        /// </summary>
        /// <returns></returns>
        public Number Abs() =>
            (A.Pow() + B.Pow()).Sqrt();

        /// <summary>
        /// Checks whether <see cref="Abs"/> is smaller than <paramref name="toCompare"/> (-1), equal (0), or grater (1) without calculating square roots
        /// </summary>
        /// <param name="toCompare"></param>
        /// <returns></returns>
        public int CompareAbs(Number toCompare)
        {
            var absSquared = A.Pow() + B.Pow();

            return absSquared.CompareTo(toCompare.Pow());
        }

        /// <summary>
        /// Plus <c>operator</c> <c>override</c>
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static Complex operator +(Complex a, Complex b)
        {
            return new Complex(a.A + b.A, a.B + b.B);
        }

        /// <summary>
        /// Minus <c>operator</c> <c>override</c>
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static Complex operator -(Complex a, Complex b)
        {
            return new Complex(a.A - b.A, a.B - b.B);
        }

        /// <summary>
        /// Multiplication <c>override</c>
        /// </summary>
        /// <param name="c1"></param>
        /// <param name="c2"></param>
        /// <returns></returns>
        public static Complex operator *(Complex c1, Complex c2)
        {
            Number
                a = c1.A,
                b = c1.B,
                c = c2.A,
                d = c2.B;

            return new Complex(a * c - b * d, b * c + a * d);
        }
    }
}
