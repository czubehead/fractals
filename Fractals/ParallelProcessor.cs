﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Fractals
{
    /// <summary>
    ///     Allows an intuitive processing of fractals
    /// </summary>
    public class ParallelProcessor
    {
        #region Delegates

        /// <summary>
        ///     Function used for iterating <paramref name="z" />
        /// </summary>
        /// <param name="z">input from last iteration</param>
        /// <param name="c">constant, number in Gaussian plane we're trying to evaluate</param>
        /// <returns></returns>
        public delegate Complex Iterate(Complex z, Complex c);

        #endregion

        /// <summary>
        ///     Result which means given c is a member of fractal set
        /// </summary>
        public const int BelongsToSet = -1;

        /// <summary>
        ///     <see cref="IteratingFunction" /> for Mandelbrot set
        /// </summary>
        public static readonly Iterate MandelbrotIterator = (z, c) => z * z + c;

        private int _calculated;

        private int _toCalculate = 1;

        /// <summary>
        ///     Initializes a new instance of <see cref="ParallelProcessor" /> with given iterating function
        /// </summary>
        /// <param name="func">future <see cref="IteratingFunction" /></param>
        public ParallelProcessor(Iterate func)
        {
            if (func == null) throw new ArgumentNullException(nameof(func));

            IteratingFunction = func;
        }

        /// <summary>
        ///     Iterating function in use. Initialized from constructor
        /// </summary>
        public Iterate IteratingFunction { get; }

        /// <summary>
        ///     Gets whether <see langword="this" /> is processing data
        /// </summary>
        public bool IsBusy { get; private set; }

        /// <summary>
        ///     Progress of processing, ranging from 0 to 1 inclusive
        /// </summary>
        public double Progress => _calculated == 0 ? 0 : //avoid DividedByZeroException
            _calculated * 1D / _toCalculate;

        /// <summary>
        ///     Count of iterations to do
        /// </summary>
        public int Iterations { get; set; } = 20;

        /// <summary>
        ///     Initial Z used for incrementation
        /// </summary>
        public Complex InitialZ { get; set; }

        /// <summary>
        ///     Processes <paramref name="payload" /> by iterating it by <see cref="IteratingFunction" /> by number of
        ///     <see cref="Iterations" />
        /// </summary>
        /// <param name="payload">payload to process</param>
        /// <returns>
        ///     Array of payload results. Same size as <paramref name="payload" />, each index is a result of iterating.
        ///     <see cref="BelongsToSet" /> if it stayed in range or number of iteration under which it fell out.
        /// </returns>
        public Task<int[,]> ProcessPayload(Complex[,] payload)
        {
            int width = payload.GetLength(0),
                height = payload.GetLength(1);
            var results = new int[width, height];

            _calculated = 0;
            _toCalculate = width - 1;

            IsBusy = true;

            return Task.Run(() =>
            {
                Parallel.For(0, width, x =>
                {
                    for (var y = 0; y < height; y++)
                    {
                        var z = InitialZ;

                        var c = payload[x, y];
                        results[x, y] = BelongsToSet;

                        for (var i = 0; i < Iterations; i++)
                        {
                            z = IteratingFunction(z, c);

                            if (z.CompareAbs(2) < 0) continue;

                            //out
                            results[x, y] = i;
                            break;
                        }
                    }

                    Interlocked.Increment(ref _calculated);
                });

                IsBusy = false;
                _calculated = _toCalculate;//ensure 100% completion

                return results;
            });
        }
    }
}