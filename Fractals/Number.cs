﻿using System;
using System.CodeDom;
using System.Diagnostics;
using System.Globalization;
using System.Numerics;
using Fractals.Properties;

namespace Fractals
{
    /// <summary>
    ///     Allows operations with numbers of virtually any size.
    /// </summary>
    public struct Number : IComparable<Number>
    {
        public static readonly Number Zero = new Number(0);
        public static readonly Number One = new Number(1);

        /// <summary>
        /// </summary>
        /// <param name="integer"></param>
        /// <param name="e"></param>
        public Number(long integer, int e = 0) : this()
        {
            Integer = integer;
            E = e;
        }

        /// <summary>
        ///     Scientific notation ×10^
        /// </summary>
        public int E { get; set; }

        /// <summary>
        ///     The integer part
        /// </summary>
        public long Integer { get; set; }

        /// <summary>
        ///     Trims ending zeros from <see cref="Fractals.Number.Integer" /> and
        ///     adds them up to <see cref="Fractals.Number.E" />
        /// </summary>
        public void Trim()
        {
            if (Integer == 0)
            {
                E = 0;
                return;
            }
            while (Integer % 10 == 0)
            {
                Integer /= 10;
                E++;
            }
        }

        #region comparasions

        /// <summary>
        /// Compares to another <see cref="Number"/>
        /// </summary>
        /// <param name="other"></param>
        /// <returns><c>-1</c> if other is bigger, <c>0</c> if they are equal and <c>1</c> if other is smaller</returns>
        public int CompareTo(Number other)
        {
            int thisDigits = DigitCount(Integer) + E;
            int otherDigits = DigitCount(other.Integer) + other.E;

            if (thisDigits < otherDigits)
                return -1;
            if (thisDigits > otherDigits)
                return 1;

            var copy = this;
            ToSameExp(ref copy, ref other);
            if (copy.Integer < other.Integer)
                return -1;

            return copy.Integer > other.Integer
                ? 1 : 0;
        }

        /// <summary>
        /// Grater <see langword="operator"/>
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator >(Number a, Number b)
            => a.CompareTo(b) > 0;

        /// <summary>
        /// Less <see langword="operator"/>
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator <(Number a, Number b)
            => a.CompareTo(b) < 0;

        /// <summary>
        /// Grater or equal <see langword="operator"/>
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator >=(Number a, Number b)
            => a.CompareTo(b) >= 0;

        /// <summary>
        /// Less or equal <see langword="operator"/>
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator <=(Number a, Number b)
            => a.CompareTo(b) <= 0;

        /// <summary>
        /// Equality <see langword="operator"/>
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator ==(Number a, Number b)
            => a.Equals(b);

        /// <summary>
        /// Not equal operator
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool operator !=(Number a, Number b)
            => !(a == b);

        #endregion

        /// <summary>
        /// Returns a string representation of given instance in scientific notation rounded by 3 digits.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
            => ToString(3);

        /// <summary>
        ///     Returns in scientific notation
        /// </summary>
        /// <param name="maxDecimals">
        /// Count of decimals output should be rounded to. Pass -1 to avoid rounding
        /// </param>
        /// <returns>
        /// </returns>
        public string ToString(int maxDecimals)
        {
            if (maxDecimals < -1 || maxDecimals > LongHelpers.MaxDigits - 1) throw new ArgumentOutOfRangeException(nameof(maxDecimals));

            string digits = Math.Abs(Integer).ToString();

            if (digits.Length < 4)
                return Integer.ToString();

            string sign = Integer < 0 ? "-" : "";
            int e = E;
            int decimalPlaces = digits.Length - 1;

            digits = digits.Insert(1, ".");
            e += decimalPlaces;

            #region rounding

            if (maxDecimals != -1)
            {
                string decimalPart = digits.Substring(2);
                //everything behind decimal dot

                int reduce = decimalPart.Length - maxDecimals;
                //number of decimals to get rid of

                if (reduce > 0)
                {
                    //covert to decimal divided by 10^reduce . This conversion loses no quality
                    decimal d = long.Parse(decimalPart) * (decimal)1 / LongHelpers.Exp(reduce);

                    d = Math.Round(d);
                    string shortDecimal = ((long)d).ToString(CultureInfo.InvariantCulture);

                    digits = digits.Remove(2);
                    digits += shortDecimal;
                }
            }
            #endregion

            return $"{sign}{digits}E{e}";
        }

        #region operators, conversions

        /// <summary>
        ///     Adds 2 <see cref="Number" /> s
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns>
        /// </returns>
        public static Number operator +(Number a, Number b)
        {
            var result = new Number();

            a.Trim();
            b.Trim();

            if (a.E == b.E) //same exponent
            {
                result.E = a.E;
                result.Integer = a.Integer + b.Integer;
            }
            else
            {
                ToSameExp(ref a, ref b);

                result.Integer = a.Integer + b.Integer;
                result.E = a.E;
                result.Trim();
            }

            return result;
        }

        /// <summary>
        ///     Subtracts 2 <see cref="Number" />
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns>
        /// </returns>
        public static Number operator -(Number a, Number b)
        {
            b.Integer *= -1;
            return a + b;
        }

        /// <summary>
        ///     Multiplies <see cref="Number" /> by <see langword="double" />
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns>
        /// </returns>
        public static Number operator *(Number a, double b) => a * FromDouble(b);

        /// <summary>
        ///     Multiply 2 <see cref="Number" />
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns>
        /// </returns>
        public static Number operator *(Number a, Number b)
        {
            if (LongHelpers.CanMultiply(a.Integer, b.Integer))
                return NewTrimmed(a.Integer * b.Integer, a.E + b.E);

            //integers are too big to be safely multiplied
            BigInteger bigA = a.Integer;
            BigInteger bigB = b.Integer;

            var bigMult = bigA * bigB;
            var multiplied = bigMult.ReduceToNumber();

            multiplied.E += a.E + b.E;

            return multiplied;
        }

        /// <summary>
        /// Divides two <see cref="Number"/>s
        /// </summary>
        /// <param name="a">dividend</param>
        /// <param name="b">divisor</param>
        /// <returns>a/b</returns>
        public static Number operator /(Number a, Number b)
        {
            double ints = a.Integer / (b.Integer * 1D);
            Number ret = ints;
            ret.E += a.E - b.E;
            ret.Trim();

            return ret;
        }

        /// <summary>
        /// Divides two <see cref="Number"/>s
        /// </summary>
        /// <param name="a">dividend</param>
        /// <param name="b">divisor</param>
        /// <returns>a/b</returns>
        public static Number operator /(Number a, double b) =>
            a / FromDouble(b);

        /// <summary>
        ///     Makes <see cref="Number" /> from <see langword="double" />
        /// </summary>
        /// <param name="a"></param>
        public static implicit operator Number(double a) => FromDouble(a);

        /// <summary>
        ///     Makes <see cref="Number" /> from int
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public static implicit operator Number(int a) => FromInt(a);

        /// <summary>
        ///     Makes <see cref="Number" /> from <see langword="double" />
        /// </summary>
        /// <param name="a"></param>
        public static Number FromDouble(double a)
        {
            if (double.IsNaN(a) || double.IsNegativeInfinity(a) || double.IsPositiveInfinity(a))
                throw new ArgumentException("Cannot parse this value");

            // ReSharper disable once CompareOfFloatsByEqualityOperator
            if (a == 0)
                return Zero;

            var s = a.ToString(CultureInfo.InvariantCulture);
            var e = 0;
            long integer;

            var ePos = s.IndexOf("E", StringComparison.Ordinal);
            var dotPos = s.IndexOf(".", StringComparison.Ordinal);

            if (ePos >= 0) //scientific notation
            {
                e = int.Parse(s.Substring(ePos + 1));
                s = s.Remove(ePos); //done with that
            }
            if (dotPos >= 0) //is decimal
            {
                integer = long.Parse(s.Remove(dotPos, 1)); //parse without dot
                e -= s.Length - dotPos - 1;
            }
            else //integer
            {
                integer = long.Parse(s);
            }

            return NewTrimmed(integer, e);
        }

        /// <summary>
        ///     Makes <see cref="Number" /> from int
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        public static Number FromInt(int a) => FromDouble(a);

        /// <summary>
        ///     Converts <see cref="long" /> to <see cref="Number" />
        /// </summary>
        /// <param name="a"></param>
        public static implicit operator Number(long a) => NewTrimmed(a, 1);

        #endregion

        /// <summary>
        ///     Initializes a new instance of <see cref="Number" /> which is trimmed.
        /// </summary>
        /// <param name="integer"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public static Number NewTrimmed(long integer, int e = 0)
        {
            var num = new Number(integer, e);
            num.Trim();
            return num;
        }

        /// <summary>
        ///     Ten to the power <paramref name="e" />
        /// </summary>
        /// <param name="e">power of ten</param>
        /// <returns>
        /// </returns>
        private static Number Exp(int e) => NewTrimmed(1, e);


        /// <summary>
        ///     Calculates the square root
        /// </summary>
        /// <returns></returns>
        public Number Sqrt()
        {
            if (Integer < 0)
                throw new InvalidOperationException("Cannot make square root of negative number");
            if (Integer == 0L)
                return Zero;

            Number eSqrt;

            #region square root of 10^E

            if (E == 0) //sqrt(1)=1
            {
                eSqrt = FromInt(1); //1
            }
            else
            {
                if (E % 2 == 0) //even positive or negative
                {
                    eSqrt = Exp(E / 2); //sqrt(10^2k) = 10^k
                }
                else
                {
                    if (E > 0) //odd positive
                    {
                        eSqrt = Exp((E - 1) / 2);
                        Number sqrt10 = Math.Sqrt(10);
                        eSqrt *= sqrt10;
                    }
                    else //odd negative
                        eSqrt = Exp((E + 1) / 2) * (1D / Math.Sqrt(10));
                }
            }

            #endregion

            var intSqrt = Math.Sqrt(Integer);

            var res = eSqrt * intSqrt;
            //res.Trim();
            return res;
        }

        /// <summary>
        /// Returns power of <paramref name="exponent"/>
        /// </summary>
        /// <param name="exponent"></param>
        /// <returns></returns>
        public Number Pow(uint exponent = 2)
        {
            int exp = (int)exponent;

            var ret = BigInteger.Pow(Integer, exp).ReduceToNumber();
            ret.E += E * exp;

            ret.Trim();

            return ret;
        }

        /// <summary>
        ///     Compares 2 <see cref="Number" />s
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool Equals(Number obj)
        {
            return (E == obj.E) && Integer.Equals(obj.Integer);
        }

        /// <summary>
        ///     Count of <paramref name="input" /> digits
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private static int DigitCount(long input)
        {
            int digitCount = input.ToString(CultureInfo.InvariantCulture).Length;
            if (input < 0)//remove minus
                digitCount -= 1;

            return digitCount;
        }

        /// <summary>
        ///     Transforms <paramref name="a" /> and <paramref name="b" /> in such a
        ///     way that they have the same E. If E difference is too high, the smaller number is set to 0
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns>
        /// </returns>
        private static void ToSameExp(ref Number a, ref Number b)
        {
            if (a.E == b.E) //same exponent
                return;

            Number smaller = a,
                bigger = b;
            var aIsSmaller = true;

            if (a.E > b.E)
            {
                smaller = b;
                bigger = a;
                aIsSmaller = false;
            }

            var dE = bigger.E - smaller.E; //exponent difference, always > 1

            var maxMultiplier = bigger.Integer != 0 ?
                Math.Abs((LongHelpers.Exp(LongHelpers.MaxDigits) - 1) / bigger.Integer) :
                LongHelpers.Exp(LongHelpers.MaxDigits) - 1;
            //max multiplier with respect to limits of long

            var multiply = true; //whether multiply bigger.Integer by 10^dE

            if (dE > DigitCount(maxMultiplier))
            {
                //multiplying would cause Integer overflow

                var error = dE - DigitCount(maxMultiplier); //always positive
                //number by which must dE be reduced

                smaller = smaller.ReduceIntegerDigits(error, false);
                if (smaller.Integer == 0)
                    multiply = false;

                dE -= error;

                if (dE > DigitCount(maxMultiplier))
                    Debug.WriteLine("fuck");
            }

            if (dE > DigitCount(maxMultiplier))
                Debug.WriteLine("fuck");

            if (multiply)
            {
                //multiply integer by 10^dE in order to reduce dE
                bigger.Integer *= LongHelpers.Exp(dE); //destroy the difference between Es
                bigger.E -= dE;
            }

            #region write to references

            if (aIsSmaller)
            {
                a.Integer = smaller.Integer;
                b.E = smaller.E;

                b.Integer = bigger.Integer;
                b.E = bigger.E;
            }
            else
            {
                a.Integer = bigger.Integer;
                a.E = bigger.E;

                b.Integer = smaller.Integer;
                b.E = smaller.E;
            }

            #endregion
        }

        /// <summary>
        /// Reduces number of <see cref="Integer"/> digits by rounding and increasing <see cref="E"/>
        /// </summary>
        /// <param name="reduce">number of digits to reduce</param>
        /// <param name="trim">wheter trim result</param>
        /// <returns></returns>
        public Number ReduceIntegerDigits(int reduce, bool trim = true)
        {
            if (reduce < 0)
                throw new ArgumentOutOfRangeException(nameof(reduce), Resources.Number_ReduceIntDigits_must_be_positive);
            if (reduce == 0)
                return this;

            var length = DigitCount(Integer);

            if (length < reduce || reduce > LongHelpers.MaxDigits)
            {
                //even rounding the smaller wouldn't solve the problem
                //or rounding needed is too high
                //round it to 0

                return new Number(0, E + reduce);
            }

            var sm = Integer / (LongHelpers.Exp(reduce) * 1D);
            sm = Math.Round(sm);

            var ret = new Number((long)sm, E + reduce);
            if (trim)
                ret.Trim();
            return ret;
        }
    }
}