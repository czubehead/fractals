﻿using System;
using System.Diagnostics.CodeAnalysis;
using Fractals;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FractalsTests
{
    [TestClass]
    public class NumTests
    {
        [TestMethod]
        public void PlusTest()
        {
            //normal scale
            var a = new Number(1000000); //10^6
            var b = new Number(1, 6); //10^6

            var plus = a + b;
            Assert.AreEqual(new Number(2, 6), plus);

            //small+large
            a = new Number(1, 5);
            b = new Number(1, -3);

            Assert.AreEqual(Number.FromDouble(100000.001), a + b);

            //too big magnitude jump to handle
            a = new Number(1, 20);
            b = new Number(1, -15);

            Assert.AreEqual(a, a + b);

            //very small negative
            a = new Number(-15, -45);
            b = new Number(6, -39);

            Assert.AreEqual(new Number(5999985, -45), a + b);
        }

        [TestMethod]
        public void MinusTest()
        {
            var a = Number.One; //1
            var b = new Number(1, 1); //10

            var minus = a - b;
            Assert.AreEqual(new Number(-9), minus);
        }

        [TestMethod]
        public void MultiplyTest()
        {
            var a = new Number(-2, -2); //0.02
            var b = new Number(-5, 1); //50

            Assert.AreEqual(Number.One, a * b);

            a = new Number(-8, -15);
            b = new Number(3, 54);

            Assert.AreEqual(new Number(-24, 39), a * b);

            a = new Number(-22222222222);
            b = new Number(98989898989);

            Assert.AreEqual("-2.200E21", (a * b).ToString());
        }

        [TestMethod]
        public void FromDoubleTest()
        {
            Number a = -1.0153E-10;

            Assert.AreEqual(new Number(-10153, -14), a);
        }

        [TestMethod]
        public void ToStringTest()
        {
            var a = new Number(1598);

            Assert.AreEqual("1.598E3", a.ToString());

            a = new Number(-15, 1);

            Assert.AreEqual("-15", a.ToString());

            a = new Number(-1598, 12);

            Assert.AreEqual("-1.598E15", a.ToString());

            a = new Number(-665942, 6);

            Assert.AreEqual("-6.659E11", a.ToString());

            a *= -1;

            Assert.AreEqual("6.659E11", a.ToString());

            a = new Number(-219977553306689113, 4);
            Assert.AreEqual("-2.200E21", a.ToString());
        }

        [TestMethod]
        public void SqrtTest()
        {
            var input = new[]
            {
                new Number(25, 100),
                new Number(25, -100),
                new Number(25, 101),
                new Number(25, -101),
                new Number(25, 600),
                new Number(25, -600),
                new Number(25, 601),
                new Number(25, -601),
                Number.Zero
            };
            var sqrt10 = Math.Sqrt(10);
            var results = new[]
            {
                new Number(5, 50),
                new Number(5, -50),
                new Number(5, 50)*sqrt10,
                new Number(5, -50)*(1D/sqrt10),
                new Number(5, 300),
                new Number(5, -300),
                new Number(5, 300)*sqrt10,
                new Number(5, -300)*(1D/sqrt10),
                Number.Zero
            };

            for (var i = 0; i < input.Length; i++)
            {
                var square = input[i];
                var expected = results[i];

                var root = square.Sqrt();
                Assert.AreEqual(expected, root, $"source[{i}]: {square} : {root}");
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ReduceDigitsTest()
        {
            Number a = new Number(123456789, 3);
            a = a.ReduceIntegerDigits(3);

            Assert.AreEqual(new Number(123457, 6), a);

            //impossible
            a = new Number(1, 3);
            a = a.ReduceIntegerDigits(3);

            Assert.AreEqual(new Number(0, 6), a);

            //negative
            a = new Number(1, 3);
            a = a.ReduceIntegerDigits(-2);
        }

        [TestMethod]
        public void PowTest()
        {
            Number a = new Number(8, 3);

            Assert.AreEqual(Number.NewTrimmed(4096, 12), a.Pow(4));
        }

        [TestMethod]
        public void ComparisonTest()
        {
            Number smaller = Number.NewTrimmed(1234, -3);//1.34
            Number bigger = Number.NewTrimmed(13, -1);//1.3

            Assert.IsTrue(smaller < bigger);

            Assert.IsTrue(bigger > smaller);


            Number a = new Number(100);
            Number b = new Number(1, 2);

            Assert.IsTrue(a >= b && a <= b);
        }
    }
}