﻿using Fractals;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FractalsTests
{
    [TestClass]
    public class ComplexTests
    {
        [TestMethod]
        public void PlusTest()
        {
            var a = new Complex(1, 4); 
            var b = new Complex(2, 3); 

            var plus = a + b;
            Assert.AreEqual(new Complex(3, 7), plus);
        }

        [TestMethod]
        public void MinusTest()
        {
            var a = new Complex(1, 4);
            var b = new Complex(2, 3);

            var minus = a - b;
            Assert.AreEqual(new Complex(-1, 1), minus);
        }

        [TestMethod]
        public void MultiplyTest()
        {
            var a = new Complex(3, 2);
            var b = new Complex(7, -4);

            var by = a * b;
            Assert.AreEqual(new Complex(29, 2), by);
        }

        [TestMethod]
        public void AbsTest()
        {
            var a = new Complex(3, 4);

            var abs = a.Abs();
            Assert.AreEqual(new Number(5), abs);
        }

        [TestMethod]
        public void AbsCompareTest()
        {
            var a=new Complex(1,1);

            Assert.AreEqual(1, a.CompareAbs(1));

            Assert.AreEqual(-1, a.CompareAbs(2));

            a =new Complex(3,4);
            Assert.AreEqual(0,a.CompareAbs(5));
        }
    }
}